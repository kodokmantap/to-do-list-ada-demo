//
//  CardListViewController.swift
//  To Do List
//
//  Created by Yulibar Husni on 28/02/19.
//  Copyright © 2019 Appfish. All rights reserved.
//

import UIKit

class CardListViewController: UIViewController {
    
    var card: CardViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        card = childViewControllers.first as! CardViewController

        updateCard()
        
        // Do any additional setup after loading the view.
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func viewWillAppear(_ animated: Bool) {
        updateCard()
    }
    
    func updateCard() {
        card?.nameOutlet.text = UserDefaults.standard.string(forKey: "name")
        card?.addressOutlet.text = UserDefaults.standard.string(forKey: "address")
        card?.emailOutlet.text = UserDefaults.standard.string(forKey: "email")
        card?.phoneNumberOutlet.text = UserDefaults.standard.string(forKey: "phone")
        card?.websiteOutlet.text = UserDefaults.standard.string(forKey: "website")
        
        print("Card List Updated")
    }

}
