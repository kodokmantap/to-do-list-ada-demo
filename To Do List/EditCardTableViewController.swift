//
//  EditCardTableViewController.swift
//  To Do List
//
//  Created by Yulibar Husni on 28/02/19.
//  Copyright © 2019 Appfish. All rights reserved.
//

import UIKit

class EditCardTableViewController: UITableViewController {
    var sourceData: [String] = ["","","","","",""]

    var card: CardViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        card = childViewControllers.first as! CardViewController

        nameOutlet.text = UserDefaults.standard.string(forKey: "name")
        addressOutlet.text = UserDefaults.standard.string(forKey: "address")
        emailOutlet.text = UserDefaults.standard.string(forKey: "email")
        phoneNumber.text = UserDefaults.standard.string(forKey: "phone")
        websiteOutlet.text = UserDefaults.standard.string(forKey: "website")

    }
    
    @IBOutlet weak var nameOutlet: UITextField!
    @IBOutlet weak var addressOutlet: UITextField!
    @IBOutlet weak var emailOutlet: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var websiteOutlet: UITextField!

    
    @IBAction func nameEdit(_ sender: UITextField) {
        UserDefaults.standard.set(nameOutlet.text, forKey: "name")
        card?.updateCard()
    }
    
    
    @IBAction func addressEdit(_ sender: UITextField) {
        UserDefaults.standard.set(addressOutlet.text, forKey: "address")
        card?.updateCard()
    }
    
    @IBAction func emailEdit(_ sender: UITextField) {
        UserDefaults.standard.set(emailOutlet.text, forKey: "email")
        card?.updateCard()
    }
    
    @IBAction func phoneNumberEdit(_ sender: UITextField) {
        UserDefaults.standard.set(phoneNumber.text, forKey: "phone")
        card?.updateCard()
    }
    
    @IBAction func websiteEdit(_ sender: UITextField) {
        UserDefaults.standard.set(websiteOutlet.text, forKey: "website")
        card?.updateCard()
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sendCardData" {
            if let destination = segue.destination as? CardViewController {
                //data masukin sini
                destination.destinationCardData = sourceData
                print("debug")
                print(sourceData)
//                print(destination.destinationCardData)
            }
        }
//         Get the new view controller using segue.destination.
//         Pass the selected object to the new view controller.

    
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */
    }
    
    
}

