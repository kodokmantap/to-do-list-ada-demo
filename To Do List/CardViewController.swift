//
//  CardViewController.swift
//  To Do List
//
//  Created by Yulibar Husni on 28/02/19.
//  Copyright © 2019 Appfish. All rights reserved.
//

import UIKit

class CardViewController: UIViewController {

    var destinationCardData: [String?] = ["","","","","",""]

    override func viewDidLoad() {

        super.viewDidLoad()
        
        nameOutlet.text = UserDefaults.standard.string(forKey: "name")
        addressOutlet.text = UserDefaults.standard.string(forKey: "address")
        emailOutlet.text = UserDefaults.standard.string(forKey: "email")
        phoneNumberOutlet.text = UserDefaults.standard.string(forKey: "phone")
        websiteOutlet.text = UserDefaults.standard.string(forKey: "website")

    }
    
    
    @IBOutlet weak var nameOutlet: UILabel!
    @IBOutlet weak var workTitleOutlet: UILabel!
    @IBOutlet weak var addressOutlet: UILabel!
    @IBOutlet weak var phoneNumberOutlet: UILabel!
    @IBOutlet weak var websiteOutlet: UILabel!
    @IBOutlet weak var emailOutlet: UILabel!
    
    func updateCard() {
        nameOutlet.text = UserDefaults.standard.string(forKey: "name")
        addressOutlet.text = UserDefaults.standard.string(forKey: "address")
        emailOutlet.text = UserDefaults.standard.string(forKey: "email")
        phoneNumberOutlet.text = UserDefaults.standard.string(forKey: "phone")
        websiteOutlet.text = UserDefaults.standard.string(forKey: "website")
        
    }

}
