//
//  ToDoListTableViewController.swift
//  To Do List
//
//  Created by Yulibar Husni on 27/02/19.
//  Copyright © 2019 Appfish. All rights reserved.
//

import UIKit

class ToDoListTableViewController: UITableViewController {
    
    var items: [String] = []
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "toDoCell")
        
        cell.textLabel?.text = items[indexPath.row]
        cell.accessoryType = .checkmark
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "toDoCell")
        let text = 
        cell.textLabel?.text = items[indexPath.row]
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setEditing(true, animated: false)
        // Do any additional setup after loading the view, typically from a nib.
        
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        let itemsObject = UserDefaults.standard.object(forKey: "items")
        
        if let tempItems = itemsObject as? [String] {
            
            items = tempItems
            
        }
        tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        /*if editingStyle == UITableViewCellEditingStyle.delete {
         
         items.remove(at: indexPath.row)
         
         table.reloadData()
         
         UserDefaults.standard.set(items, forKey: "items")
         
         }*/
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

